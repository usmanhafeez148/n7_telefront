define(function(require){
	/* App code in JavaScript */
	console.log('application runs');
	var $ = require('jquery'),
	_ = require('lodash'),
	monster = require('monster'),
	toastr = require('toastr');

	var app = {
		name: 'demo',

		i18n: {
			'en-US': { customCss: false }
		},

		requests: {
			/* List of APIs */

			'google.getUser': {
				apiRoot: 'http://api.google.com/'
				url: 'users',
				verb: 'PUT'
			}
		},

		subscribe: {
			 'demo.getUser': 'getUser'
		},

		load: function(callback){
			var self = this;

			self.initApp(function() {
				callback && callback(self);
			});
		},

		initApp: function(callback) {
			var self = this;

			monster.pub('auth.initApp', {
				app: self,
				callback: callback
			});
		},

		render: function(container){
			var self = this,
			demoTemplate = $(monster.template(self, 'layout')),
			parent = _.isEmpty(container) ? $('#monster_content') : container;

			self.bindEvents(demoTemplate);

			(parent)
				.empty()
				.append(demoTemplate);
		}

		bindEvents: function(template) {
			var self = this;

			template.find('#search').on('click', function(e) {
				self.searchNumbers(650, 15, function(listNumbers) {
					var results = monster.template(self, 'results', { numbers: listNumbers});

					template
						.find('.results')
						.empty()
						.append(results);
				});
			});
		},

		searchNumbers: function(pattern, size, callback) {
			var self = this;

			monster.request({
				resource: 'demo.listNumbers',
				data: {
					pattern: pattern,
					size: size
				},
				success: function(listNumbers) {
					callback && callback(listNumbers.data);
				}
			});
		}
	};

	return app;
});